# Wardrobify

Team:

* Adam - Hats
* Naomi - Shoes

## Design

## Shoes microservice

This project uses Django back-end service to build the base for how our information will be held and how to access it when trying to append it to the page.
We used Javascript and React to pull the information from our back end service and display it on the site. Bootrap was implemented for basic styling purposes.

## Hats microservice

For the hats microservice, we utilized a simple ‘Hat’ model with the characteristics specified in the project guidelines. This included fabric, style, color, a picture URL, and a method for linking our entries (through a foreign key) to a corresponding Location entry in the wardrobe microservice. 

This ‘linking’ was accomplished by the creation of a ‘LocationVO’ model or virtual object with closet name and import href properties. By utilizing this model and the RESTful format of the existing APIs in the wardrobe microservice, we were able to tie each Hat object to its corresponding Location entity.

Ultimately, this setup allows us to simplify the creation of Hat objects (less information required) and prevent any synchronization issues from arising due to multiple databases containing Location entities.
