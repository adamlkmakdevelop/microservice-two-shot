import React, { useState , useEffect} from "react";

const ShoeForm = (props) => {
    const [manufacturer, setManufacturer] = useState('');
    const [modelName, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [pictureURL, setpictureURL] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);


    const handleSubmit = async (event) => {
        event.preventDefault();
        const shoeData = {
            manufacturer: manufacturer,
            modelName: modelName,
            color: color,
            pictureURL: pictureURL,
            bin: bin,
            bins: bins
        };

        const shoeURL = 'http://localhost:8080/api/shoes/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(shoeData),
            headers: {
              'Content-Type': 'application/json',
            },
        }; 

        const response = await fetch(shoeURL, fetchConfig);
        console.log(response)
        if(response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            setManufacturer('');
            setModelName('');
            setColor('');
            setpictureURL('');
            setBin('');
        }
        }

        const getBins = async () => {
            const binURL = "http://localhost:8100/api/bins/"
            const response = await fetch(binURL)
            if (response.ok) {
                const {bins} = await response.json();
                setBins(bins)
            }
        }

        useEffect(() => {
            getBins();
          }, [])

    const handleManufacturerChange = (event) => {
        const { value } = event.target;
        setManufacturer(value);
    }

    const handleModelNameChange = (event) => {
        const { value } = event.target;
        setModelName(value);
    }

    const handleColorChange = (event) => {
        const { value } = event.target;
        setColor(value);
    }

    const handlePictureChange = (event) => {
        const { value } = event.target;
        setpictureURL(value);
    }

    const handleBinChange = (event) => {
        const { value } = event.target;
        setBin(value);
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Track a new shoe in your wardrobe!</h1>
              <form onSubmit={handleSubmit} id="track-shoe-form">
                <div className="form-floating mb-3">
                  <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" id="manufacturer-id" className="form-control" />
                  <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleModelNameChange} value={modelName} placeholder="Model Name" required type="text" id="model-name-id" className="form-control" />
                  <label htmlFor="modelName">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleColorChange} value={color} placeholder="Color" type="text" id="shoe-color-id" className="form-control" />
                  <label htmlFor="color">Shoe Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePictureChange} value={pictureURL} placeholder="Picture Url" required type="text" id="shoe-pic-id" className="form-control" />
                  <label htmlFor="pictureUrl">URL for shoe image </label>
                </div>
                <div className="mb-3">
                  <select onChange={handleBinChange} value={bin} required className="form-select" id="bin-id">
                    <option value="">Choose a Bin</option>
                    {bins.map(bin => {
                      return (
                        <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Track Your Shoes!</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default ShoeForm;
