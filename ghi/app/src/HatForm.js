import React, { useState } from 'react';

function HatForm(props) {
  const [fabric, setFabric] = useState('');
  const [style, setStyle] = useState('');
  const [color, setColor] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  // const [location, setLocation] = useState('');

  const [closetName, setClosetName] = useState('');
  const [sectionNumber, setSectionNumber] = useState('');
  const [shelfNumber, setShelfNumber] = useState('');

  function handleClosetNameChange(event) {
    const { value }  = event.target;
    setClosetName(value);
  }

  console.log(closetName)

  function handleSectionNumberChange(event) {
    const { value }  = event.target;
    setSectionNumber(value);
  }

  console.log(sectionNumber)

  function handleShelfNumberChange(event) {
    const { value }  = event.target;
    setShelfNumber(value);
  }

  const closetNames = []
  for(let location of props.locations){
    if(closetNames.indexOf(location.closet_name) === -1){
      closetNames.push(location.closet_name)
    }
  }

  const sectionNumbers = []
  for(let location of props.locations){
    if(location.closet_name == closetName && sectionNumbers.indexOf(location.section_number) == -1){
      sectionNumbers.push(location.section_number)
    }
  }

  const shelfNumbers = []
  for(let location of props.locations){
    if(location.closet_name == closetName && location.section_number == sectionNumber && shelfNumbers.indexOf(location.shelf_number) == -1 ){
      shelfNumbers.push(location.shelf_number)
    }
  }

  let selectedLocation = undefined
  for(let location of props.locations){
    if(location.closet_name == closetName && location.section_number == sectionNumber && location.shelf_number == shelfNumber){
      selectedLocation = location
    }
  }

  console.log(selectedLocation)

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      fabric: fabric,
      style: style,
      color: color,
      picture_url: pictureUrl,
      location: selectedLocation,
    };

    const locationId = selectedLocation.id

    const locationUrl = `http://localhost:8090/api/locations/${locationId}/hats/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);
      setFabric('');
      setStyle('');
      setColor('');
      setPictureUrl('');
      setClosetName('');
      setSectionNumber('');
      setShelfNumber('');
      selectedLocation = undefined;
      props.getHats()
    }
  }

  function handleFabricChange(event) {
    const { value } = event.target;
    setFabric(value);
  }

  function handleStyleChange(event) {
    const { value } = event.target;
    setStyle(value);
  }

  function handleColorChange(event) {
    const { value } = event.target;
    setColor(value);
  }

  function handlePictureChange(event) {
    const { value } = event.target;
    setPictureUrl(value);
  }

  // function handleLocationChange(event) {
  //   const { value } = event.target;
  //   setLocation(value);
  // }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Track a New Hat!</h1>
          <form onSubmit={handleSubmit} id="track-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" id="fabric" className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStyleChange} value={style} placeholder="Style" required type="text" id="style" className="form-control" />
              <label htmlFor="style">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} value={color} placeholder="Color" type="text" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureChange} value={pictureUrl} placeholder="Picture Url" required type="text" id="pictureUrl" className="form-control" />
              <label htmlFor="pictureUrl">URL for Hat Picture</label>
            </div>
            {/* <div className="mb-3">
              <select onChange={handleLocationChange} value={location} required className="form-select" id="location">
                <option value="">Choose a location</option>
                {props.locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>{location.closet_name}</option>
                  )
                })}
              </select>
            </div> */}
            <div className="mb-3">
              <select onChange={handleClosetNameChange} value={closetName} required className="form-select" id="closetName">
                <option value="">Choose a closet</option>
                {closetNames.map(closet => {
                  return (
                    <option key={closet} value={closet}>{closet}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleSectionNumberChange} value={sectionNumber} required className="form-select" id="sectionNumber">
                <option value="">Choose a section</option>
                {sectionNumbers.map(section => {
                  return (
                    <option key={section} value={section}>Section {section}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleShelfNumberChange} value={shelfNumber} required className="form-select" id="shelfNumber">
              <option value="">Choose a shelf</option>
                {shelfNumbers.map(shelf => {
                  return (
                    <option key={shelf} value={shelf}>Shelf {shelf}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Track Your Hat!</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
