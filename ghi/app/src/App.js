import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList'
import HatForm from './HatForm'
import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';



function App(props) {
    const [ hats, setHats ] = useState([]);
    const [ locations, setLocations ] = useState([]);
    const [ shoes, setShoes ] = useState([]);
    // const [ bin, setBin ] = useState([]);

    async function getHats() {
      const response = await fetch('http://localhost:8090/api/hats/');
      if (response.ok) {
        const data = await response.json();
        setHats(data.hats);
      } else {
        console.error('An error occurred fetching hat data')
      }
    };

    async function getLocations() {
      const response = await fetch('http://localhost:8100/api/locations/');
      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
      } else {
        console.error('An error occurred fetching location data')
      }
    };

    const getShoes = async () => {
      const response = await fetch('http://localhost:8080/api/shoes/');
      if (response.ok) {
        const { shoes } = await response.json();
        setShoes(shoes);
      } else {
        console.error('An error occurred fetching shoes data');
      }
    };

    useEffect(() => {
      getHats();
      getLocations();
      getShoes();
    }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />


          <Route path="/hats/">
            <Route index element={<HatList hats={hats} getHats={getHats} />} />
            <Route path="/hats/new" element={<HatForm getHats={getHats} locations={locations} />} />
          </Route>


          <Route path="/shoes/">
            <Route index element={<ShoeList shoes={shoes} />} />
            <Route path="new/" element={<ShoeForm shoes={shoes} getShoes={getShoes} />} />
          </Route>


        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
