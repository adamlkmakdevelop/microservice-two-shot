import React from "react";

const ShoeList = (props) => {
    return (
        <table className="shoe-table table table-striped">
        <thead>
          <tr>
            <th>Model Name</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Bin</th>
            <th>Picture URL</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(shoe => {
            return (
              <tr key={ shoe.href }>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.bin.closet_name }</td>
                <td>{ shoe.picture_url }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
}

export default ShoeList