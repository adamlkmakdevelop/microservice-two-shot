import React, { useState } from 'react';

function HatList(props) {
  console.log(props.hats)

  async function deleteHat(event) {
    const id=event.target.value
    const locationUrl = `http://localhost:8090/api/hats/${id}`;
    const fetchConfig = {
      method: "delete"}
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      // window.location.reload(false)
      props.getHats()
    }}

  return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style</th>
            <th>Color</th>
            <th>Picture_URL</th>
            <th>Closet Name</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map(hat => {
            return (
              <tr key={hat.href}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style }</td>
                <td>{ hat.color }</td>
                <td>{ hat.picture_url }</td>
                <td>{ hat.location.closet_name }</td>
                <td><button onClick={deleteHat} value={hat.id}>Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default HatList;
