from django.urls import path

from .api_views import api_list_all_shoes, api_show_shoe_detail, api_list_shoes_in_bin


urlpatterns = [
    path("shoes/", api_list_all_shoes, name="api_list_all_shoes"),
    path("shoes/<int:id>/", api_show_shoe_detail, name="api_show_shoe_detail"),
    path("bins/<int:bin_vo_id>/shoes/", api_list_shoes_in_bin, name="api_list_shoes_in_bin")
]
