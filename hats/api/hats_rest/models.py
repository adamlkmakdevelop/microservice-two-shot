from django.db import models
from django.urls import reverse

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)

    def __str__(self):
        return self.closet_name

class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        "locationVO",
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.style

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})
